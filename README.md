# Backend CRUD API REST

_En esta práctica se va a aprender a crar un servicio web el cual proporcione un API RESTful sobre HTTP que se comporte como un CRUD de una base de datos, es decir Create, Read, Update and Delete_

_Para realizar dicho servicio utilizaremos tecnologías MEAN: NodeJS para la parte del back-end, Express como framework y MongoDB como gestor de base de datos_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋

Lo primero para comenzar con la práctica es tener unos programas básicos que se necesitarán a lo largo de la práctica:

_Visual Studio Code_
```
sudo snap install postman
```

_Postman_
```
sudo snap install --classic code
```


### Instalación 🔧

Para comenzar a tener un entorno de desarrollo ejecutandose, comenzaremos instalando _Node JS_ con los siguientes comandos: 


```
sudo apt install npm
sudo npm clean -f
sudo npm i -g n
sudo n stable
```

Ahora seguimos con la instalación del gestor de repositorios, _git_ y lo configuramos con nuestros datos de acceso:

```
sudo apt install git
git config --global user.name nombreUsuario
git config --global user.email correo@alu.ua.es
```

A continuación instalamos _Express_ que nos facilitará enormemente la gestión de métodos y recursos HTTP
```
npm -i -S express
```

También instalaremos _Morgan_ que será el motor de regitro del servidor
```
npm i -S morgan
```

## Ejecutando las pruebas ⚙️

### Primer Proyecto

Para crear nuestro primer pequeño proyecto, por ejemplo una carpeta vacia, en este caso llamada _api-rest_ y subirla a un repositorio remoto como puede ser _bitbucket_

Se empieza creando el repositorio local: 
```
git init
echo "#Backend CRUD API REST” > README.md
```
Y éste lo conectamos con el repositorio remoto, asigandole un nombre:
```
git remote -v
git remote add nombre https://usuario@bitbucket.org/usuario/api-rest.git
```

_Ahora si comenzamos con el proyecto_
```
npm init
```
Y completamos lo que nos piden y con el visual code creamos nuestro servidor Web que funcionará con Node JS.
Una vez lo tengamos creado para iniciarlo lo haremos mediante el siguiente código:
```
node index.js
```
Ahora implementamos express a nuestro servidor en el archivo index.js y lo inicaremos para probarlo

### Creación de un API RESTFul Básico

Para llevar a cabo esta parte deberemos instalar nodemon:
```
npm -i -D nodemon
```
Una vez instalado lo agregamos a nuestro package.json mediante la siguiente línea
```
"start": "nodemon index.json"
```

Ahora lanzamos el servidor y probaremos que funcione.

Una vez comprobado que funciona sin problemas subiremos a nuestro repositorio remoto los archivos
```
git add . 
git commit -m "Primer API REST con Express y JSON"
git push
```

### API REST tipo CRUD

Ahora podremos escribir ya un pequeño servicio que atienda los principales métodos
HTTP que darán soporte a nuestra interfaz RESTful (GET, POST, PUT, DELETE).

Primero cambiaremos nuestro código añadiendo las acciones CRUD que necesitemos.
Ahora en el postman ejecutaremos cada método get, post... que hayamos definido y comprobaremos su funcionamiento. Una vez hecho todo eso exportaremos la colección creada y la subiremos al repositorio remoto.

### Servicio Web CRUD mediante API RESTFul y MongoDB

Como indica el título usaremos MongoDB por lo que lo prinmero será su instalación
```
sudo apt install -y mongodb
```
Y procederemos a iniciarlo
```
sudo systemctl start mongodb
```
Finalmente, en nuestro proyecto tendremos que instalar la biblioteca mongodb para trabajar con la base de datos 
```
cd node/api-rest
npm i -S mongodb
npm i -S mongojs
```
La instalaremos en nuestra ruta donde tenemos el repositorio local en este caso _node/api-rest_

Una vez tenemos todas las herramientas y bibliotecas que necesitamos, escribiremos en el archivo index.js el código JavaScript que implementará la funcionalidad esperada de nuestro servidor: API REST simple para los métodos HTML que se describieron anteriormente.

Una vez tengamos todo hehco iniciaremos nuestro servidor y mongoDB para comprobar su buen funcionamiento y lo probaremos con postman para comprobar que todos las acciones CRUD funcionan 

Y finalmente veamos que no tenemos ningún problema subiremos a nuestro repostorio local los archivos.

## Analice las pruebas end-to-end 🔩

_Explica qué verifican estas pruebas y por qué_

```
Proporciona un ejemplo
```

## Y las pruebas de estilo de codificación ⌨️

_Explica qué verifican estas pruebas y por qué_

```
Proporciona un ejemplo
```

## Despliegue 📦

_Agrega notas adicionales sobre cómo hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
* **Fulanito Detal** - *Documentación* - [fulanitodetal](#fulanito-de-tal)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quiénes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.